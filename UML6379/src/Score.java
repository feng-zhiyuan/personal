import java.util.*;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
public class Score {
	public static void main(String a[]) throws FileNotFoundException, IOException {
		Properties p=new Properties();
		p.load(new FileInputStream("/src/total.properties"));
		Enumeration<?> fileName = p.propertyNames();
		//获取配置文件的键值
		double ScoreBefore = Integer.parseInt(p.getProperty("before"));
		double ScoreBase = Integer.parseInt(p.getProperty("base"));
		double ScoreTest = Integer.parseInt(p.getProperty("test"));
		double ScoreProgram = Integer.parseInt(p.getProperty("program"));
		double ScoreAdd = Integer.parseInt(p.getProperty("add"));
		//导入html文件
		File file_small = new File("/src/small.html");
		File file_all = new File("/src/all.html");
		getScores(file_small, file_all, ScoreBefore, ScoreBase,ScoreTest, ScoreProgram,
				ScoreAdd);
	}
	//获取总分
	public static void getScores(File small_File, File all_File, double ScoreBefore, double ScoreBase,
			double ScoreTest, double ScoreProgram, double ScoreAdd) throws IOException {
		int Before1 = 0;
		int Base1 = 0;
		int Test1 = 0;
		int Program1 = 0;
		int Add1 = 0;
		//使用jsoup爬取html文件内容
			org.jsoup.nodes.Document document = Jsoup.parse(small_File, "UTF-8");
			org.jsoup.nodes.Document document2 = Jsoup.parse(all_File, "UTF-8");
			//计算总分的方法
				double AllBefore = Before1 / ScoreBefore * 100 * 0.25;
				double AllBase = Base1 / ScoreBase * 100 * 0.3 * 0.95;
				double AllTest = Test1 / ScoreTest * 100 * 0.2;
				double AllProgram = Program1 / ScoreProgram * 100 * 0.1;
				double AllAdd = Add1 / ScoreAdd * 100 * 0.05;
				double AllScore = (AllBefore + AllBase + AllTest + AllProgram
						+ AllAdd)+6;
				System.out.println(AllScore);
			}
		
}
